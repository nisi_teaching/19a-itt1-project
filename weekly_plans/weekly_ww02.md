---
Week: 02
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 02 ITT1 Project

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Participate in the poster presentation
* Hand in OLA12

### Learning goals

The student can:

* Present a project using poster presentation

## Deliverables

* All group members have participated in the poster presentation
* All groups have handed OLA12 in on wiseflow

## Schedule Monday 2020-01-06

* 8:15 Introduction to the day, general Q/A session

    * Poster presentation (OLA 12)
        * [OLA12 requirements](https://gitlab.com/EAL-ITT/19a-itt1-project/blob/master/docs/OLA/OLA12.pdf) (same document as the one on wiseflow)

* 12:15 Poster setup

    * Re-arrange tables and chairs
    * Setup posters on cardboaqrd walls 
    
* 13:00 Poster presentations in B2.05/B2.49

    * There will be cardboard walls available for the posters.   

    * Groups should be ready to present at 13:00, meaning that posters and other relevant material is prepared before.

    * At the poster presentation students present the results of the entire project and if possible a presentation of the **physical product**   

    * The entire presentation has a duration of 1,5 hours, where students will present to anyone interested.  

    * During the presentation students will present to teachers for maximum 10 minutes.

* 14:30 Cleanup

    * Put tables and chairs back as they were before

* 15:30 End of day


## Hands-on time

* Get everything ready for the poster presentation

## Comments

\pagebreak