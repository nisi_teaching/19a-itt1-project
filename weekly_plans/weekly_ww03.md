---
Week: 03
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 03 ITT1 Project

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Participate in the poster presentation 2nd attempt
* Hand in OLA12 2nd attempt

### Learning goals

The student can:

* Present a project using poster presentation

## Deliverables

* All group members that did not attend 1st try, have participated in the poster presentation
* All group members that did not attend 1st try, have handed in OLA12 2nd attempt on wiseflow

## Schedule Monday 2020-01-06

* 8:15 Introduction to the day

    * Poster presentation (OLA 12 2nd attempt)
        * [OLA12 requirements](https://gitlab.com/EAL-ITT/19a-itt1-project/blob/master/docs/OLA/OLA12_2nd_attempt.pdf) (same document as the one on wiseflow)

* 9:00 Recap Q&A session

    If you have stuff from project you would like us to recap before exam this is the time to do it.

* 11:00 Poster setup for OLA12 2nd attempt

    * Re-arrange tables and chairs
    * Setup posters on cardboaqrd walls 
    
* 12:30 Poster presentations in B2.05/B2.49

    * There will be cardboard walls available for the posters.   

    * Groups should be ready to present at 12:30, meaning that posters and other relevant material is prepared before.

    * At the poster presentation students present the results of the entire project and if possible a presentation of the **physical product**   

    * The entire presentation has a duration of 1,5 hours, where students will present to anyone interested.  

    * During the presentation students will present to teachers for maximum 10 minutes.

* 13:30 Cleanup

    * Put tables and chairs back as they were before

* 15:30 End of day


## Hands-on time

* Get everything ready for the poster presentation

## Comments

\pagebreak