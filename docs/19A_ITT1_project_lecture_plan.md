---
title: '19A ITT1 Project'
subtitle: 'Lecture plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait19, 19A
* Name of lecturer and date of filling in form: NISI, ILES 2019-06-01
* Title of the course, module or project and ECTS: Semester project, 5 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.

--------- ------ ---------------------------------------------
 INIT      Week  Content
--------- ------ ---------------------------------------------
NISI/ILES  37    Phase 1: Introduction

                 Groups

                 Projects 

                 Gitlab

                 Risk management (pre mortem)

NISI/ILES  38    Phase 1: Idea and research
                 
                 Project plan

                 Team meeting 
                 
                 Induction 

NISI/ILES  39    Phase 1: Idea and research

                 research current systems 
                 
                 continue task work 
                 
NISI/ILES  40    Phase 1: Idea and research
                 
                 OLA11 1st attempt

                 Project overview
                 
                 continue task work

NISI/ILES  41    Phase 2: Development

                 continue task work

NISI/ILES  42    No lectures this week

NISI/ILES  43    Phase 2: Development

                 continue task work

NISI/ILES  44    Phase 2: Development

                 continue task work

NISI/ILES  45    Phase 2: Development

                 continue task work

NISI/ILES  46    Phase 3: Consolidation

                 continue task work

NISI/ILES  47    Phase 3: Consolidation

                 continue task work

NISI/ILES  48    Phase 4: Prototype

                 continue task work

NISI/ILES  49    Phase 4: Prototype

                 continue task work

NISI/ILES  50    Phase 4: Prototype

NISI/ILES  51    Finalize report

                 OLA13 1st attempt: Project report submitted and approved

NISI/ILES  2     Project presentations

                 OLA 12 1st attempt: Final presentation of Semester 1 project 

---------------------------------------------------------


# General info about the course, module or project

## The student’s learning outcome

Can be found in the [2018 curriculum](https://www.ucl.dk/globalassets/01.-uddannelser/a.-videregaende-uddannelser/internationale-uddannelser/it-technology-ap/dokumenter/curriculum-2018-it-technology.pdf?nocache=0065f079-10eb-4d06-bbd4-18fd7178b2f5)

## Content

The course is formed around several projects, and will include relevant technology from the curriculum. It is intended for cross-topic consolidation.

## Method

The project will be divided into phases that build upon each other. The idea is to create a systems that works from sensor to presentation while the students practice project management and learn operational skills.

![project phases](../docs/images/itt1_project_phases.PNG)

## Equipment

None specified at this time as it is dependent on the topic chosen.

## Projects with external collaborators (proportion of students who participated)

Project 1: Wireless charging (Copenhagen Technologies)  
Project 2: Inductive Load Monitoring (Sydfyns El Forsyning)  
Project 3: Unmanned Submarine (Copenhagen Technologies)

Project descriptions are on [itt1-project gitlab pages](https://eal-itt.gitlab.io/19a-itt1-project/pdf.html)

## Test form/assessment
The project includes 3 obligatory learning activities (OLA).

See exam catalogue for details on obligatory learning activities.

## Other general information
