# Obligatory Learning Activity 13 (OLA13)

### Course 

1st Semester project

### Class 

OEAIT19EIA and OEAIT19EIB

### Topic

Project report (Final)

## Information

For rules and regulations about OLA, see the semester description on itslearning, in the folder, ressources/general education documents.

## Exercise instructions

OLA13 is the final project report of semester 1 project.
The report should contain a description of the entire 1st semester project.
The report is a group handin and follows the structure of the report template attached to the flow on wiseflow. 

## Hand-in on wiseflow

The hand-in is a pdf report handed in on wiseflow. The structure and content should resemble the below list:

    1. Preface
    2. Summary
    3. Table of contents
    4. Introduction
    5. Materials and methods
        * Hardware
        * Software
        * Project management
    6. Tests and results
    7. Discussion
    8. Further development
    9. Conclusion
    10. References (use harvard style)
    11. Appendix (not assessed, not part of page count)

    **Use the file "Project report template - report title - Author names.docx" attached to wiseflow as extra material**

    The report template contains further explanation of each element in the content list.

    In technical reports it is important to be objective and exclude subjective stuff. Have this in mind while writing.

    Images/screenshots should always have a caption, images not referenced in the text is redundant and should not be part of the report.

    Do not include large images/code snippets, include whats relevant in the text and the rest in the appendix.

    We expect maximum 20 standard pages (1 page = 2400 chars) + appendix. Note that this is a group hand-in.

Follow-up
============

After the hand-in period, the teachers will review the reports. Results are communicated on wiseflow.